<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class FileUploadController extends Controller
{
    public function handle_upload(Request $request)
    {
	$validator = Validator::make($request->all(), [
	    "file" => "required|mimetypes:application/pdf"
	]);
	if($validator->passes())
	{
		if($this->pdfService->searchFor('Proposal'))
		{
		    $file_size=$request->file('file')->getSize();
		    $file_name=$request->file('file')->getClientOriginalName();
		    $previous_file_record=FileDb::where('name',$file_name)->where('file_size',$file_size)->first();
		    if($previous_file_record)
		    {

		    }
		}
	}
	else
	{
		abort(422);
	}
    }
}
