<?php

namespace App\Http\Controllers;

use App\Models\Company;
use App\Models\Country;
use App\Models\User;
use Illuminate\Http\Request;

class CountryController extends Controller
{
    public function test()
    {
	$country_name = 'Canada';
	$country = Country::where('name', 'like', '%'.$country_name.'%')->first();
	echo "<table><th>$country->name</th><tbody>";
	foreach ($country->companies as $company)
	{
	    echo "<tr><td>$company->name</td></tr><br>";
	    foreach ($company->users as $user)
	    {
		echo "<tr><td>$user->name</td></tr><br>";
	    }
	}
	$query = "SELECT users.name,c.name,uc.created_at FROM users
join user_companies uc on users.id = uc.user_id
join companies c on c.id = uc.company_id
join countries c2 on c.country_id = c2.id
where c2.name like 'Canada'";
    }
}
